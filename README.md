# How to train

For GPU you may to `export TF_CPP_MIN_LOG_LEVEL=2` to disable rudiment logs

```shell script
wget https://pjreddie.com/media/files/yolov3.weights -o model_data/yolov3.weights
python convert.py
python generate.py
python kmeans.py
python train.py
```

# How to detect figures

To detect figures on test image:
```shell script
python detect_test_image.py
```

To detect figures on image:
```shell script
python detect_image.py
```

To detect figures on video:
```shell script
python detect_video.py
```

# How to tune 'figures.cfg'

* change line batch to [`batch=64`](model_data/figures.cfg#L2)
* change line subdivisions to [`subdivisions=16`](model_data/figures.cfg#L3)
* change line max_batches to (`classes*2000` but not less than number of training images, and not less than `6000`), f.e. [`max_batches=6000`](model_data/figures.cfg#L16) if you train for 3 classes
* change line steps to 80% and 90% of max_batches, f.e. [`steps=4800,5400`](model_data/figures.cfg#L18)
* set network size `width=416 height=416` or any value multiple of 32: model_data/yolov3_figures.cfg#L8-L9
* change line `classes=80` to your number of objects in each of 3 `[yolo]`-layers:
    * [Line 606](model_data/figures.cfg#L606)
    * [Line 692](model_data/figures.cfg#L692)
    * [Line 779](model_data/figures.cfg#L779)
* change [`filters=255`] to filters=(classes + 5)x3 in the 3 `[convolutional]` before each `[yolo]` layer, keep in mind that it only has to be the last `[convolutional]` before each of the `[yolo]` layers.
    * [Line 599](model_data/figures.cfg#L599)
    * [Line 685](model_data/figures.cfg#L685)
    * [Line 722](model_data/figures.cfg#L772)

# Links
* [darknet](https://github.com/AlexeyAB/darknet)
* [keras-yolo3](https://github.com/qqwweee/keras-yolo3)
* [keras-yolo3-custom](https://github.com/michhar/keras-yolo3-custom)
