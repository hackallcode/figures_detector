import math
import os
import random
import sys

from PIL import Image, ImageDraw

CLASSES = 5


def count_borders(coords):
    min_x = sys.maxsize
    min_y = sys.maxsize
    max_x = 0
    max_y = 0

    for c in coords:
        min_x = min(min_x, c[0])
        min_y = min(min_y, c[1])
        max_x = max(max_x, c[0])
        max_y = max(max_y, c[1])

    return [min_x, min_y, max_x, max_y]


def polygon_coords(x=50, y=50, n=4, r=30, a=45):
    a = math.radians(a)
    res = []

    for i in range(n):
        x_coord = x + r * math.sin(a + 2 * math.pi * i / n)
        y_coord = y + r * math.cos(a + 2 * math.pi * i / n)
        res.append((x_coord, y_coord))

    return res


def random_color():
    return random.randint(0, 200), random.randint(0, 200), random.randint(0, 200)


def random_fill():
    color_type = random.randint(0, 10)
    outline = None
    fill = None
    if color_type % 2 == 0 or color_type == 10:
        outline = random_color()
    if color_type % 2 != 0 or color_type == 10:
        fill = random_color()
    return (outline, fill)


def draw_random(draw, x, y, w, h, min_scale):
    n = random.randint(1, CLASSES)
    r = random.randint(int(min(w, h) * min_scale / 2), int(min(w, h) / 2))
    x = random.randint(x + r, x + w - r)
    y = random.randint(y + r, y + h - r)

    (outline, fill) = random_fill()

    if n == 1:
        borders = [x - r, y - r, x + r, y + r]
        draw.ellipse(borders, outline=outline, fill=fill)
    elif n == 2:
        a = random.randint(0, 359)
        coords = polygon_coords(x, y, 6, r, a)
        coords = [coords[0], coords[2], coords[3], coords[5]]
        borders = count_borders(coords)
        draw.polygon(coords, outline=outline, fill=fill)
    else:
        a = random.randint(0, 359)
        coords = polygon_coords(x, y, n, r, a)
        borders = count_borders(coords)
        draw.polygon(coords, outline=outline, fill=fill)

    # draw.rectangle(borders, outline=(0,0,0), width=2)
    return n, borders


def generate(set_name='train', count=100, max_size=128, min_scale=0.25):
    random.seed()
    figures = 9
    count = int(count / figures)

    wd = os.getcwd()
    os.makedirs(f"{wd}/{set_name}", exist_ok=True)
    f = open(f"{wd}/{set_name}.txt", "w")

    for c in range(1, 1 + figures):
        print(set_name, c)
        for i in range(count):
            # Generate ranges for each figure
            ranges = []
            max_count = math.ceil(math.sqrt(c))
            max_width = int(max_size / max_count)
            for x in range(max_count):
                for y in range(max_count):
                    ranges.append((max_width * x, max_width * y))

            # Draw figure in different ranges
            img = Image.new('RGB', (max_size, max_size), color=(255, 255, 255))
            draw = ImageDraw.Draw(img)
            positions = []
            for _ in range(c):
                r = random.choice(ranges)
                ranges.remove(r)
                (n, border) = draw_random(draw, r[0], r[1], max_width, max_width, min_scale)
                positions.append((n, border))
            del draw

            image_path = f"{wd}/{set_name}/{c}_{i:04d}.jpg"
            img.save(image_path, "JPEG")
            f.write(image_path)
            for pos in positions:
                f.write(f" {','.join([str(int(a)) for a in pos[1]])},{pos[0] - 1}")
            f.write('\n')

    f.close()


generate('data/train', CLASSES * 2000)
generate('data/test', 90, max_size=512, min_scale=0.5)
