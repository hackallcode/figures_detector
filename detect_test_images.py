import argparse
import glob
import ntpath
import os

from PIL import Image

from yolo import YOLO


def detect_img(yolo):
    os.path.isdir('data/test')
    os.makedirs('data/test_results', exist_ok=True)

    test_images = glob.glob('data/test/*')
    for img in test_images:
        try:
            image = Image.open(img)
        except:
            print(f'Impossible to open {img}')
            continue
        else:
            r_image = yolo.detect_image(image)
            r_image.save(f'data/test_results/{ntpath.basename(img)}')
    yolo.close_session()


args = None

if __name__ == '__main__':
    # class YOLO defines the default value, so suppress any default here
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    '''
    Command line options
    '''
    parser.add_argument(
        '--model', type=str,
        help='path to model weight file, default ' + YOLO.get_defaults("model")
    )

    parser.add_argument(
        '--anchors', type=str,
        help='path to anchor definitions, default ' + YOLO.get_defaults("anchors")
    )

    parser.add_argument(
        '--classes', type=str,
        help='path to class definitions, default ' + YOLO.get_defaults("classes")
    )

    parser.add_argument(
        '--gpu_num', type=int,
        help='Number of GPU to use, default ' + str(YOLO.get_defaults("gpu_num"))
    )

    args = parser.parse_args()
    detect_img(YOLO(**vars(args)))
